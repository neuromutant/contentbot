import { steps } from '../lib/steps';

const low      = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('./db.json');
const db      = low(adapter);

enum keyMap {
	DELETE_PATH = 'delete_path',
	CONTENT     = 'content',
	BULK_MODE   = 'bulk_mode',
	STEP        = 'step',
	LAST_SEARCH = 'last_search',
	STACK       = 'stack'
}

interface ISearchResult {
	content : string
	type    : string
	title   : string
	caption : string
}

export class Db {
  private id: number
  public step: number;
  constructor(id) {
  	this.id   = id;
  	this.step = this.get_step() || steps.ADD_CONTENT;
  }
  write_key(key, content) {
  	db.set(`${this.id}.${key}`, content).write();
  }
  get_key<T>(key):T {
  	return db.get(`${this.id}.${key}`).value();
  }
  push_to_key(key, content) {
  	return  db.get(`${this.id}.${key}`)
	  		  .push(content)
			  .write();
  }
  //deletions
  set_delete_path(path) {
	  this.write_key(keyMap.DELETE_PATH, path);
  }
  get_delete_path() {
  	return this.get_key(keyMap.DELETE_PATH);
  }
  delete_from_db(file_id) {
  	const path = this.get_delete_path();
  	db.get(`${this.id}.${keyMap.CONTENT}.${path}`)
  		.remove({ content : file_id })
  		.write();
  }
  //bulk mode
  set_bulk_mode(mode) {
	  this.write_key(keyMap.BULK_MODE, mode);
  }
  get_bulk_mode() :boolean {
  	return this.get_key<boolean>(keyMap.BULK_MODE); 
  }
  //steps
  set_step(step: steps): void {
  	this.step = step;
  	this.write_key(keyMap.STEP, step);
  }
  get_step() : steps {
	  return this.get_key(keyMap.STEP);
  }
  // emoji/text content
  set_emoji_content(emoji, content) :void {
  	const key          = `${keyMap.CONTENT}.${emoji}`;
  	const contentArray = this.get_key(key);
  	if(!Array.isArray(contentArray)) {
  		this.write_key(key, []);
  	}
  	this.push_to_key(key, content);
  }
  get_emoji_content(emoji):void  {
  	const key = `${keyMap.CONTENT}.${emoji}`;
  	return this.get_key(key);
  }
  //search settings
  set_last_seach(keys): void {
  	this.write_key(keyMap.LAST_SEARCH, keys);
  }
  get_last_seach() : string[] {
  	return this.get_key(keyMap.LAST_SEARCH);
  }
  // predictive search
  search_emoji_content(emoji):ISearchResult[] {
  	let fullContent = this.get_key(keyMap.CONTENT);

  	if(fullContent !== undefined) {
  		let keys       = Object.keys(fullContent);
  		let resultKeys = [];
  		let result     = [];
  		if(emoji) {
  			for(let key of keys) {
  				if(key === emoji) {
  					resultKeys = [emoji];
  					break;
  				} else if(key.includes(emoji)) {
  					resultKeys.push(key);
  				}
  			}
  		} else {
  			resultKeys = this.get_last_seach();
  			if(resultKeys === undefined) {
  				resultKeys = [];
  			}
  		}
  		this.set_last_seach(resultKeys);
     
  		for(let key of resultKeys) {
  			result.push(...fullContent[key]);
  		}
  		return result;
  	}
  }
  // simple stack operations
  set_stack(anything) :void {
	  this.write_key(keyMap.STACK, anything);
  }
  get_stack():any {
	  return this.get_key(keyMap.STACK);
  }
}
