export enum steps {
    ADD_CONTENT = 1,
    SELECT_FOLDER = 2,
    DELETE_CONTENT_SELECT_FOLDER = 3,
    DELETE_CONTENT_SELECT_FILE = 4
}
