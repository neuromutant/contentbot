
import {Telegraf, Context}           from 'telegraf';
import { Db }                        from '../db/db';

export interface IBotContext extends Context {
  db: Db
}

const bot = new Telegraf<IBotContext>(process.env.BOT_TOKEN);

bot.use(async(ctx, next) => {
	ctx.db = new Db(ctx.from.id);
	next();
});

export {bot};