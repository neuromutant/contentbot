import { InlineQueryResult } from 'telegraf/typings/core/types/typegram';

// generates proper types for inline query answer
export const stackFun = (stack:InlineQueryResult[], entry, id) => {
	id = id.toString();
	return({
		photo: () => stack.push({
			id,
			type         : 'photo',
			photo_file_id: entry.content,
			caption      : entry.caption
		}),
		sticker: () => stack.push({
			id,
			type           : 'sticker',
			sticker_file_id: entry.content,
		}),
		animation: () => stack.push({
			id,
			type       : 'gif',
			gif_file_id: entry.content
		}),
		document: () => stack.push({
			id,
			type            : 'document',
			document_file_id: entry.content,
			title           : entry.title,
			caption         : entry.caption
		}),
		voice: () => stack.push({
			id,
			type         : 'voice',
			voice_file_id: entry.content,
			title        : entry.title
		}),
		video: () => stack.push({
			id,
			type         : 'video',
			title        : entry.title,
			video_file_id: entry.content,
			caption      : entry.caption
		}),
	}[entry.type]());
};