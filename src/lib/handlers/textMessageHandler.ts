import { steps } from '../steps';

export const textMessageHandler = async(ctx) => {
	const text = ctx.message['text'];
	if (ctx.db.step === steps.SELECT_FOLDER) {
		const stack = ctx.db.get_stack();
		ctx.db.set_emoji_content(text, stack);
		ctx.db.set_step(steps.ADD_CONTENT);
		ctx.reply('your content was saved');
	}
	
	if(ctx.db.step === steps.DELETE_CONTENT_SELECT_FOLDER) {
		const content = ctx.db.get_emoji_content;
		if(!content) {
			ctx.reply('not found');
			return;
		}
		ctx.db.set_step(steps.DELETE_CONTENT_SELECT_FILE);
		ctx.reply('now send me a file that you want to delete');
	}
};