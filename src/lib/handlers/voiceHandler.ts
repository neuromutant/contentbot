import { steps } from '../steps';

const get_username = (ctx) => {
	if (ctx.message.forward_sender_name) {
		return ctx.message.forward_sender_name;
	} 
	return ctx.update.message.from.username || 'unknown';
};

export const voiceHandler = async(ctx) => {
	if (ctx.db.step !== steps.ADD_CONTENT) return;

	ctx.db.set_stack({
		content: ctx.message.voice.file_id,
		type   : 'voice',
		title  : get_username(ctx)
	});
  
	ctx.reply('Please send me an emoji/text for your voice');
	ctx.db.set_step(steps.SELECT_FOLDER);
};