import { steps }       from '../steps';

export const animationHandler = async(ctx) => {
	if (ctx.db.step !== steps.ADD_CONTENT) return;
	ctx.db.set_stack({
		content: ctx.message.animation.file_id,
		type   : 'animation',
		caption: ctx.message.caption || ''
	});
	ctx.reply('Please send me an emoji/text for your gif');
	ctx.db.set_step(steps.SELECT_FOLDER);
};