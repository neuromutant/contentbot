import { steps } from '../steps';

export const videoHandler = async(ctx) => {
	if (ctx.db.step !== steps.ADD_CONTENT) return;
	
	ctx.db.set_stack({
		content: ctx.message.video.file_id,
		type   : 'video',
		title  : ctx.message.video.file_name || 'unknown',
		caption: ctx.message.caption || ''
	});

	ctx.reply('Please send me an emoji/text for your video');
	ctx.db.set_step(steps.SELECT_FOLDER);
};