import { steps } from '../steps';

export const cancelHandler = (ctx) => {
	ctx.db.set_step(steps.ADD_CONTENT);
	ctx.reply('cancelled');
};