import { steps }       from '../steps';

const get_audio_title = (ctx) => {
	if (ctx.message.audio.performer && ctx.message.audio.title) {
		return `${ctx.message.audio.performer} ${ctx.message.audio.title}`;
	}
	return ctx.message.audio.file_name;
};

export const audioHandler = async(ctx) => {
	if (ctx.db.step !== steps.ADD_CONTENT) return;

	ctx.db.set_stack({
		content: ctx.message.audio.file_id,
		type   : 'document',
		title  : get_audio_title(ctx)
	});
	ctx.reply('Please send me an emoji/text for your audio');
	ctx.db.set_step(steps.SELECT_FOLDER);
};