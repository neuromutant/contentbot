import { InlineQueryResult } from 'telegraf/typings/core/types/typegram';
import { stackFun }          from '../contentStack';

const typeMap = {
	img     : 'photo',
	sticker : 'sticker',
	gif     : 'animation',
	document: 'document',
	voice   : 'voice',
	video   : 'video',
};

const extractTag = (text:string) => {
	const splitted = text.split('#');
	if(splitted.length > 1) {
		return [splitted[0], typeMap[splitted[1]]];
	}
	return [splitted[0], false];
};

export const inlineQueryHandler = async(ctx) => {
	const text    = ctx.update.inline_query.query.trim();
	let id        = ctx.update.update_id;
	const [clean] = extractTag(text);
	let entries   = ctx.db.search_emoji_content(clean.trim());

	let stack: InlineQueryResult[] = [];
	if (Array.isArray(entries)) {
		for (let entry of entries) {
			id++;
			stackFun(stack, entry, id);
		}
	}
	ctx.answerInlineQuery(stack, {
		cache_time : 10,
		is_personal: true
	});
};