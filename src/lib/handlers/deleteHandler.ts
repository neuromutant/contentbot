import { steps } from '../steps';

export const deleteHandler = (ctx) => {
	ctx.db.set_step(steps.DELETE_CONTENT_SELECT_FOLDER);
	ctx.reply('send me emoji or text that you want to delete');
};