import { steps } from '../steps';

export const stickerHandler = async(ctx) => {
	if (ctx.db.step !== steps.ADD_CONTENT)  return;

	ctx.db.set_stack({
		content: ctx.message.sticker.file_id,
		type   : 'sticker'
	});
	ctx.reply('Please send me an emoji/text for your sticker');
	ctx.db.set_step(steps.SELECT_FOLDER);
};