import { steps } from '../steps';

export const photoHandler = async(ctx) => {
	if (ctx.db.step !== steps.ADD_CONTENT) return;
	ctx.db.set_stack({
		content: ctx.message.photo.pop().file_id,
		type   : 'photo',
		caption: ctx.message.caption || ''
	});
	ctx.reply('Please send me an emoji/text for your photo');
	ctx.db.set_step(steps.SELECT_FOLDER);
};