require('dotenv').config();

import { bot}            from './lib/bot';
import { animationHandler,
	voiceHandler,
	videoHandler,
	textMessageHandler,stickerHandler,
	photoHandler,inlineQueryHandler,
	audioHandler,
	deleteHandler,
	cancelHandler,
	videoNoteHandler } from './lib/handlers/handlers';

process.on('unhandledRejection', function(reason) {
	//logs automatically to pm2
	console.log(reason);
});

bot.command('/cancel', cancelHandler);
bot.command('/delete', deleteHandler);

bot.on('video_note', videoNoteHandler);
bot.on('photo', photoHandler);
bot.on('video', videoHandler);
bot.on('animation', animationHandler);
bot.on('voice', voiceHandler);
bot.on('audio', audioHandler);
bot.on('sticker', stickerHandler);
bot.on('message', textMessageHandler);
bot.on('inline_query', inlineQueryHandler);

bot.launch();
